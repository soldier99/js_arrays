let cb = (element,k) =>{
    let a ;
    element === k ? a = true : a = false ;
    return a ;
}



function find(elements, cb, toBeSearched) {
    for(let i = 0;i<elements.length;i++){
       let a = cb(elements[i], toBeSearched);
       if(a){
           return elements[i];
       }
    }
}

module.exports = {
    find,
    cb
}