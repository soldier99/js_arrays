
let recFn = input =>{
    //let a = input.length;
    let n ;
    if(!Array.isArray(input)){
        n = input ;
    }else{
        n = recFn(input[0])
    }
    return n;
}

function flatten(elements) {
    let empArr = [];
    for(let i = 0;i<elements.length;i++){
        let n = recFn(elements[i])
        empArr.push(n)
    }
    return empArr;
    // Flattens a nested array (the nesting can be to any depth).
}

module.exports = flatten ;